import matplotlib.pyplot as plt

size_sequence = [10, 50, 100, 150, 200, 250, 300, 350, 400, 450, 500, 550, 600, 650]
parallelise_GPU = [0.11, 0.55, 1.11, 1.66, 2.22, 2.81, 3.40, 4.02, 4.60, 5.28, 5.95, 6.65, 7.40, 8.10]
parallelise_CPU = [0.02, 0.11, 0.36, 0.97, 2.07, 4.03, 6.84, 11.1, 17.05, 26.63, 39.40, 55.62, 80.90, 112]
non_parallelise_NW = [0.02, 1.15, 9.94, 33., 78., 150., 264., 416., 624., 886., 1225., 1623., 2130., 2704.]


plt.figure()

plt.subplot(311)
plt.plot(size_sequence, parallelise_GPU)
plt.title("Time to compute Needleman and Wunch matrix in seconds: ")
plt.ylabel("Parallelize on GPU")
plt.subplot(312)
plt.plot(size_sequence, parallelise_CPU)
plt.ylabel("Parallelize on CPU")
plt.subplot(313)
plt.plot(size_sequence, non_parallelise_NW)
plt.ylabel("Classic implementation")
plt.xlabel("Length of sequences")
plt.show()
