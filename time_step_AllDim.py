import matplotlib.pyplot as plt
import numpy as np
import math
from sympy import binomial

# With python2.7

def CalculProcessingUnitRequired(dimension, timeStep, lenSequence):
    total = 0
    coefficient_to_add = (timeStep-1) // lenSequence
    #print("Time step : {}".format(timeStep))	
    for q in range(coefficient_to_add + 1):
        coefficient = int((-1)**(q) * binomial(dimension + timeStep - q*lenSequence - 2, timeStep - q*lenSequence - 1) * binomial(dimension, q))
        #print("coefficient : {} : {}".format(q, coefficient))
        total += coefficient
    #print("total : {}\n".format(total))
    return total


def getNumberProcessingUnitRequired(lenSequence, dimension):
    number_processing_unit = []
    for i in range(1, (dimension * (lenSequence - 1) + 2)):
        number_processing_unit.append(CalculProcessingUnitRequired(dimension, i, lenSequence))
    return number_processing_unit



if __name__ == '__main__':
    dimension = 5
    lenSequence1 = 100
    lenSequence2 = 200
    lenSequence3 = 300
    print("Dimension : {}".format(dimension))

    number_processing_unit1 = getNumberProcessingUnitRequired(lenSequence1, dimension)
    number_processing_unit2 = getNumberProcessingUnitRequired(lenSequence2, dimension)
    number_processing_unit3 = getNumberProcessingUnitRequired(lenSequence3, dimension)



    print("Max number of cells computed with size {} : {}".format(lenSequence1, max(number_processing_unit1)))	
    print("Max number of cells computed with size {} : {}".format(lenSequence2, max(number_processing_unit2)))	
    print("Max number of cells computed with size {} : {}".format(lenSequence3, max(number_processing_unit3)))	


    print("Max number of cells computed / m^(N-1) -> {}".format(float((lenSequence1))**(dimension-1) / max(number_processing_unit1)))
    print("Max number of cells computed / m^(N-1) -> {}".format(float((lenSequence2))**(dimension-1) / max(number_processing_unit2)))
    print("Max number of cells computed / m^(N-1) -> {}".format(float((lenSequence3))**(dimension-1) / max(number_processing_unit3)))


    x = np.arange(1, 1000)
    number_time_step_parallelize = dimension*x - (dimension - 1)
    number_time_step_non_parallelize = x**dimension

    fig1 = plt.subplot(2, 1, 1)
    plt.title('Difference number of time step if parallelization or not in dimension ' + str(dimension) + ':')
    plt.plot(x, number_time_step_parallelize)
    plt.ylabel('With parallelization')
    plt.subplot(2, 1, 2)
    plt.plot(x, number_time_step_non_parallelize)
    plt.ylabel('Without parallelization')
    plt.xlabel('Len of sequences')
    plt.show()

    plt.plot(range(1, len(number_processing_unit1) + 1), number_processing_unit1)
    plt.plot(range(1, len(number_processing_unit2) + 1), number_processing_unit2)
    plt.plot(range(1, len(number_processing_unit3) + 1), number_processing_unit3)
    plt.legend(['len_sequences : ' + str(lenSequence1), 'len_sequences : ' + str(lenSequence2), 'len_sequences : ' + str(lenSequence3)], loc='upper left')
    plt.title('Number of elements computed in dimension ' + str(dimension) + ':')
    plt.xlabel('Time step')
    plt.ylabel('Number of elements computed')
    plt.show()


    size_sequences = np.arange(1, 1000)
    max_number_processing_unit_required = []
    for i in range(len(size_sequences)):
        max_number_processing_unit_required.append(CalculProcessingUnitRequired(dimension, int(((size_sequences[i]*dimension) - (dimension - 1))) / 2 + 1, size_sequences[i]))
    plt.plot(size_sequences, max_number_processing_unit_required)
    plt.title('Maximum Number of elements computed in dimension ' + str(dimension) + ':')
    plt.xlabel('Len of sequences')
    plt.ylabel('Max Number of elements computed')
    plt.show()
